package MavenProject_02.Maven02;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import dropDownPage.DropDown;
import fileUploadPackage.FileUpload;
import utils.testData;

public class testCasesForDropDown {

WebDriver driver = new ChromeDriver();
	
	DropDown dropdown = new DropDown(driver);
	static testData testData;
	
	
	@Test
	public void selectFromDropDown() throws IOException, InterruptedException {
		testData = new testData();
		driver.get(testData.property.getProperty("DropDownUrl"));
		Thread.sleep(3000);
		dropdown.selectCountyByIndex(12);//Australia
		Thread.sleep(3000);
		dropdown.selectCountyByValu("12");//Baharain
		Thread.sleep(3000);
		dropdown.selectCountyByVisibleText("ARGENTINA ");		
	}

}
