package MavenProject_02.Maven02;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.json.StaticInitializerCoercer;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import RandomNumber_Page_Properties.RegistrationPage;

import utils.testData;

public class testCasesWithRandomNumbers {

	WebDriver driver = new ChromeDriver();
	
	//Create Objects of All Pages
	
		//HomePage homepage = new HomePage(driver);
		RegistrationPage registration = new RegistrationPage(driver);
		
	//	String url ="https://demo.opencart.com/";
		String fname = RandomStringUtils.randomAlphabetic(10)+" "+RandomStringUtils.randomAlphabetic(10)+" "+RandomStringUtils.randomAlphabetic(10);
		String lname = RandomStringUtils.randomAlphabetic(10)+" "+RandomStringUtils.randomAlphabetic(10)+" "+RandomStringUtils.randomAlphabetic(10);
		String email = RandomStringUtils.randomAlphabetic(10)+"@gmail.com";
		String phonenumber=RandomStringUtils.randomNumeric(11);
		String password=RandomStringUtils.randomAlphabetic(10);
		String confirmPassword=password;
		
		static testData testData;
		
		@BeforeTest
		public void beforeTestMethod() throws IOException {
	//	driver.get(url);	
		testData = new testData();
		driver.get(testData.property.getProperty("BaseUrl"));
		
			
		}
		

	//-------------Test Cases for Registration Process-----------------------------------------------------------------------------------------//
		@Test
		public void Verify_Registration_With_Positive_Value()
		{
		//privacy policy is already tick here for this test case.
		//homepage.ClickOnMyAccount();
		//homepage.ClickOnregistrationButton();
		//registration.FullRegistration("", "", email,"", "", "");
		registration.ProvideFirstName(fname);
		registration.ProvideLastName(lname);
		registration.ProvideEmailAddress(email);
		registration.ProvidePhoneNumber(phonenumber);
		registration.ProvidePassword(password);
		
		registration.ProvideConfirmPassword(confirmPassword);
		registration.ClickOnPrivacyPolicy();
		registration.ClickOnSubmitbutton();
		assertEquals(driver.getTitle(),"Your Account Has Been Created!","Registration note complte!!");
		//assertEquals(registration.verify_Error_for_Empty_Field_FirstName(),"First Name must be between 1 and 32 characters!"," Not able to verify the test case");
					
		}

}
