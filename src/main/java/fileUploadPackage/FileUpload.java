package fileUploadPackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class FileUpload {
	
	WebDriver driver;
	

	public FileUpload(WebDriver driver) {
				this. driver=driver;
		
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//INPUT[@id='fileField']")
	WebElement fileupload;
	
	
	public void fileuploadsection(String filepath) {
		fileupload.sendKeys(filepath);
		
		
		
	}

}
