package RandomNumber_Page_Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage {
	
	WebDriver driver;
	public RegistrationPage(WebDriver driver) {
		
		this.driver = driver;
		//This initElements method will create all WebElements
		PageFactory.initElements(driver, this);
	}
	
	
	//All WebElements are identified by @FindBy annotation
	
		@FindBy(xpath = "//INPUT[@id='input-firstname']")
		WebElement FirstName;

		@FindBy(xpath = "//INPUT[@id='input-lastname']")
		WebElement LastName;

		@FindBy(xpath = "//INPUT[@id='input-email']")
		WebElement Email;
		
		@FindBy(xpath = "//INPUT[@id='input-telephone']")
		WebElement Telephone;
		
		@FindBy(xpath = "//INPUT[@id='input-password']")
		WebElement Password;
		
		@FindBy(xpath = "//INPUT[@id='input-confirm']")
		WebElement ConfirmPassword;
		
		@FindBy(xpath = "//INPUT[@type='checkbox']")
		WebElement privacyPolicy;
		
		@FindBy(css = "#content > form > div > div > input.btn.btn-primary")
		WebElement SumitRegistrationButton;
		
		


		public void ProvideFirstName(String firstname ) {
			
			FirstName.sendKeys(firstname);

			
			
		}


		public void ProvideLastName(String lastname) {
			LastName.sendKeys(lastname);

		}


		public void ProvideEmailAddress(String email) {
			System.out.println("Given email is: "+ email);
			Email.sendKeys(email);
		
			
			
		}


		public void ProvidePhoneNumber(String telephone) {
			Telephone.sendKeys(telephone);
		
			
		}


		public void ProvidePassword(String password) {
			System.out.println("Given Password is: "+ password);
			Password.sendKeys(password);
			
			
		}


		public void ProvideConfirmPassword(String confirmpassword) {
			ConfirmPassword.sendKeys(confirmpassword);
			
			
		}

		public void ClickOnPrivacyPolicy() {
			privacyPolicy.click();
			
			
		}
		
		public void ClickOnSubmitbutton() {
			
			SumitRegistrationButton.click();
			
		}
		
		
	
	
}
