package MavenProject_02.Maven02;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import fileUploadPackage.FileUpload;
import utils.testData;

public class testCasesForFileUpload {
	
	WebDriver driver = new ChromeDriver();
	
	FileUpload fileupload = new FileUpload(driver);
	
	static testData testData;
	
	
	@Test
	public void fileUploadTestMethod() throws IOException {
		testData = new testData();
		driver.get(testData.property.getProperty("FileUploadUrl"));
		fileupload.fileuploadsection(testData.property.getProperty("FilePath"));
	}

}
