package dropDownPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DropDown {

	WebDriver driver;
	

	public DropDown(WebDriver driver) {
				this. driver=driver;
		
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//SELECT[@name='country']")
	WebElement SelectCountry;
	
	
	public void selectCountyByValu(String value) {
		Select select = new Select(SelectCountry);
		select.selectByValue(value);
			
	}

	public void selectCountyByIndex(int index) {
		Select select = new Select(SelectCountry);
		select.selectByIndex(index);
		
	}
	
	public void selectCountyByVisibleText(String Text) {
		Select select = new Select(SelectCountry);
		select.selectByVisibleText(Text);
		
	}
	
}
